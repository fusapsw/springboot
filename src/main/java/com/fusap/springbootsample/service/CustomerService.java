package com.fusap.springbootsample.service;

import com.fusap.springbootsample.model.Customer;
import com.fusap.springbootsample.repository.ICustomerRepository;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor=ServiceException.class)
public class CustomerService implements ICustomerService{
    @Autowired
    private ICustomerRepository customerRepository;

    @Override
    public List<Customer> getCustomers() throws ServiceException {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomer(Integer id) throws ServiceException {
        if (customerRepository.exists(id)) return customerRepository.findOne(id);
        else return  new Customer();
    }

    @Override
    public Customer deleteCustomer(Integer id) throws ServiceException {
        Customer deltedCustomer = null;
        if (customerRepository.exists(id)){
            deltedCustomer = customerRepository.findOne(id);
            customerRepository.delete(id);
        }
        else throw new ServiceException("Not found");

        return deltedCustomer;
    }

    @Override
    public Customer updateCustomer(Customer customer) throws ServiceException {
        Customer updatedCustomer = null;
        if(customerRepository.exists(customer.getId()) && isValid(customer)){
            updatedCustomer = customerRepository.findOne(customer.getId()).update(customer);
        }
        else throw new ServiceException("Not found");

        return updatedCustomer;
    }

    @Override
    public Customer createCustomer(Customer customer) throws ServiceException {
        if(isValid(customer)) {
            return customerRepository.save(customer);
        }else throw new ServiceException("No valid data for a customer");
    }

    @Override
    public boolean isValid(Customer c) {
        return c != null
            && c.getName() != null
            && c.getAddress() != null;
    }
}
