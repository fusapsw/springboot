package com.fusap.springbootsample.service;

import com.fusap.springbootsample.dtos.CustomerDto;
import com.fusap.springbootsample.model.Customer;
import org.hibernate.service.spi.ServiceException;

import java.util.List;

public interface ICustomerService {

    public List<Customer> getCustomers() throws ServiceException;

    public Customer getCustomer(Integer id) throws ServiceException;

    public Customer deleteCustomer(Integer id) throws ServiceException;

    public Customer updateCustomer(Customer customer) throws ServiceException;

    public Customer createCustomer(Customer customer) throws ServiceException;

    boolean isValid(Customer c);
}
