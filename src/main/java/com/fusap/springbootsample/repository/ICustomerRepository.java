package com.fusap.springbootsample.repository;

import com.fusap.springbootsample.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {

}
