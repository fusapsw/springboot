package com.fusap.springbootsample.base;

import javax.persistence.*;
@MappedSuperclass
public abstract class BaseEntity implements Identifiable<Integer> {
    /**
     * Base model
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}