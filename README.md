# My project's README

Prerequisites:
 java 8 installed

How to run it:
 from console java -jar spring-boot-demo.jar

We decide to use H2 as db because in that way you can run it without the need of installing an instance of a mysql, sql, oracle, etc. installed, run an script, we thought it would be more agile that way for you to test it.

The proyect was made using intellij IDEA.
We are not using maven profiles, we do not need it for this demo there is only one enviroment.

Architectural decisions:
- We opted for the following archetype a layered application with
	-Data access using Active record. we considerer using the DAO pattern but for this case an Active record will do better actually.
	-Controller: this layer will take care to recibe the http request.
	-Service: this layer will do the process of the request inside a transaction, with a rollback mecanism anytime a ServiceException arrise.
	-Model: this layer will encapsulate the bussines rules.
